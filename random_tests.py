import symmetry

def run():
  for i in range(0, 2):
    for j in range(1, 10):
      lowerBound = 0
      upperBound = 1 + i

      symmetry.find_symmetry_lines(
        "Random " + str(j) + "-Vertex with bounds [" + str(lowerBound) + ", " + str(upperBound) + "]",
        symmetry.randomIntVertices(lowerBound, upperBound, j),
      )
