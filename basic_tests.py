import math
import numpy as np
import symmetry

def run():
  symmetry.find_symmetry_lines(
    "Square",
    np.array([[0, 0], [2, 0], [2, 2], [0, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Rectangle",
    np.array([[0, 0], [4, 0], [4, 2], [0, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Rhombus",
    np.array([[0, 0], [2, 0], [1, -2], [1, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Kite",
    np.array([[0, 0], [2, 0], [1, -2], [1, 1]]),
  )

  symmetry.find_symmetry_lines(
    "Isosceles Trapezoid",
    np.array([[0, 0], [4, 0], [1, 1], [3, 1]]),
  )

  symmetry.find_symmetry_lines(
    "Trapezoid",
    np.array([[0, 0], [1, 1], [2, 1], [4, 0]]),
  )

  symmetry.find_symmetry_lines(
    "Parallelogram",
    np.array([[0, 0], [4, 0], [5, 2], [1, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Equilateral Triangle",
    np.array([[0, 0], [2, 0], [1, math.sqrt(3)]]),
  )

  symmetry.find_symmetry_lines(
    "Isosceles Triangle",
    np.array([[0, 0], [2, 0], [1, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Right Triangle Positive Slope",
    np.array([[0, 0], [1, 0], [0, 1]]),
  )

  symmetry.find_symmetry_lines(
    "Right Triangle Negative Slope",
    np.array([[0, 0], [1, 0], [1, 1]]),
  )

  symmetry.find_symmetry_lines(
    "Irregular Quad",
    np.array([[0, 0], [2, 0], [4, 2], [0, 4]]),
  )

  symmetry.find_symmetry_lines(
    "Squares Connected at One Corner",
    np.array([[0, 0], [2, 0], [2, 2], [0, 2], [-2, 0], [-2, -2], [0, -2]]),
  )
