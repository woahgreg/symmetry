from doctest import debug
from random import randint, random
import numpy as np
import matplotlib.pyplot as plt

# globals
debug

class Line:
  def __init__(self, p0, p1):
    self.p0 = p0
    self.p1 = p1

    self.isLineOfSymmetry = False
    self.slope = 0
    self.yIntercept = 0
    self.isVertical = False
    self.xIntercept = 0

    self.compute_slope_and_yintercept()

  def compute_slope_and_yintercept(self):
    # round to reduce computational rounding error for line deduplication
    if self.p0[0] - self.p1[0] == 0:
      self.isVertical = True
      self.xIntercept = round(self.p0[0], 6)
    else:
      self.slope = round((self.p0[1] - self.p1[1]) / (self.p0[0] - self.p1[0]), 6)
      self.yIntercept = round(self.p0[1] - self.slope * self.p0[0], 6)

  # compute perpendicular to line and intersecting v
  # return new line
  def compute_perpendicular(self, v):
    # compute point on perpendicular to define line
    # use arbitrary distance of 1 in one dimension depending on the line
    xp = 0
    yp = 0

    if self.isVertical:
      mp = 0 # now horizontal
      bp = v[1] - mp * v[0]
      xp = v[0] + 1
      yp = bp
    elif self.slope == 0: # horizontal
      isVertical = True
      xp = v[0]
      yp = v[1] + 1
    else:
      mp = -1 / self.slope
      bp = v[1] - mp * v[0]
      xp = v[0] + 1
      yp = mp * xp + bp

    return Line(v, [xp, yp])

  def equals(self, line):
    if self.isVertical:
      return line.isVertical and self.xIntercept == line.xIntercept
    elif line.isVertical:
      return self.isVertical and self.xIntercept == line.xIntercept
    
    return self.slope == line.slope and self.yIntercept == line.yIntercept

  def print(self):
    if self.isVertical:
      print(" * Vertical line with x-intercept", self.xIntercept)
    else:
      print(" * Slope:", self.slope, ", y-intercept:", self.yIntercept)

def plot(title, vertices, lines=None, centroid=None, midpoints=None):
  plt.cla()
  plt.title("Lines of Symmetry: " + title)
  plt.show(block=False)

  if len(vertices) > 0:
    plt.scatter(vertices[:,0], vertices[:,1], marker='o', color='blue')
  
  if lines is not None:
    for i in range(0, len(lines)):
      if lines[i].isLineOfSymmetry:
        plt.axline(lines[i].p0, lines[i].p1, color="red", linestyle=(0, (5, 5)))

  if debug:
    if centroid is not None:
      plt.scatter(centroid[0], centroid[1], marker='o', color='black')
    
    if midpoints is not None:
      plt.scatter(midpoints[:,0], midpoints[:,1], facecolors='none', edgecolors='green', linestyle='--')

  ax = plt.gca()
  ax.set_aspect('equal', adjustable='box')
  plt.draw()
  plt.pause(0.001)
  input("Press [enter] to continue.")
  print()

def equal(vertices):
  if len(vertices) < 2:
    return True

  vPrev = vertices[0]

  for v in vertices[1:]:
    if vPrev[0] != v[0] or vPrev[1] != v[1]:
      return False
    
    vPrev = v

  return True

def collinear(vertices):
  slope = None
  yIntercept = None
  isVertical = False
  xIntercept = None
  line = None

  for i, v in enumerate(vertices[:-1]):
    line = Line(v, vertices[i+1])

    if i == 0:
      if line.isVertical:
        isVertical = True
        xIntercept = line.xIntercept
      else:
        slope = line.slope
        yIntercept = line.yIntercept
    else:
      if line.isVertical:
        if not isVertical or xIntercept != line.xIntercept:
          return line, False
      else:
        if line.slope != slope or line.yIntercept != yIntercept:
          return line, False

  return line, True

def find_midpoints(vertices):
  numVerts = vertices.shape[0]
  numMidpoints = int(numVerts * (numVerts-1) / 2) # formula
  midpoints = np.empty([numMidpoints, 2]) # clean this up

  k = 0
  for i, v0 in enumerate(vertices):
    for j in range(i+1, numVerts):
      m = (v0 + vertices[j]) / 2.0
      midpoints[k, :] = m
      k += 1

  return midpoints

def get_line(p, centroid, lines):
  line = Line(p, centroid)

  # point is centroid, no lines can be found from this
  if p[0] == centroid[0] and p[1] == centroid[1]:
    return line, True
  
  # determine if line has already been tested
  for l in lines:
    if line.equals(l):
      return line, True

  return line, False

# test the line connecting the point p to the centroid 
def test_line(title, line, vertices):
  if debug:
    line.print()
    plot(title, vertices, centroid=line.p1)
    print("Drawing point and line", line.p0, line.p1)
    plt.scatter(line.p0[0], line.p0[1], marker='*', color='orange')
    plt.axline(line.p0, line.p1, color="red", linestyle=(0, (5, 5)))
    plt.draw()
    plt.pause(0.001)
    input("Press [enter] to continue.")
    print()

  vFound = np.zeros(len(vertices))

  # test that vertices are either on the line or have a reflection across it
  for i, v in enumerate(vertices):
    if vFound[i]: # ignore already found vertices
      continue

    if v[0] == line.p0[0] and v[1] == line.p0[1]: # exclude current point
      vFound[i] = 1 # mark current point true

      continue

    if debug:
      print("Drawing test vertex", v)
      plt.scatter(v[0], v[1], marker='.', color='cyan')
      plt.draw()
      plt.pause(0.001)
      input("Press [enter] to continue.")
      print()

    # test if vertex is on the line
    if line.isVertical:
      if v[0] == line.xIntercept:
        vFound[i] = 1

        if debug:
          print("Vertex is on line")

        continue
    elif v[1] == line.slope * v[0] + line.yIntercept:
      vFound[i] = 1

      if debug:
        print("Vertex is on line")

      continue

    pLine = line.compute_perpendicular(v)

    # compute intersection point between line and perpendicular
    if line.isVertical:
      xp = line.xIntercept
      yp = v[1]
    elif pLine.isVertical:
      xp = v[0]
      yp = line.yIntercept
    else:
      xp = (pLine.yIntercept - line.yIntercept) / (line.slope - pLine.slope)
      yp = line.slope * xp + line.yIntercept

    # compute x and y distances from v to intersection point
    dx = v[0] - xp
    dy = v[1] - yp

    # compute v reflected across line by adding x and y distances to
    # intersection point in the opposite direction of v
    # round to reduce computational rounding error for later comparisons
    xr = round(xp - dx, 6)
    yr = round(yp - dy, 6)

    if debug:
      print("Drawing reflected vertex", xr, yr)
      plt.scatter(xr, yr, marker='.', color='pink')
      plt.draw()
      plt.pause(0.001)
      input("Press [enter] to continue.")
      print()

    # check if this point matches any remaining vertices
    # if yes, mark found
    # if no, this line is not a line of symmetry - return empty
    found = False
    for j, vv in enumerate(vertices):
      if vFound[j]: # ignore already found vertices
        continue

      if debug:
        print("Testing vertex", vv)

      if round(vv[0], 6) == xr and round(vv[1], 6) == yr:
        vFound[i] = 1 # mark current vertex
        vFound[j] = 1 # mark reflected vertex
        found = True

        if debug:
          print("Found reflected vertex")

        break

    if not found:
      if debug:
        print("NOT a line of symmetry")

      return line

  # if all vertices have been found, return line of symmetry
  if vFound.sum() == len(vertices):
    if debug:
      print("Found line of symmetry!!!", line.p0, line.p1)

    line.isLineOfSymmetry = True

  return line

def printLinesFound(lines):
  numLines = 0
  for l in lines:
    if l.isLineOfSymmetry:
      numLines += 1

  lineStr = "lines"

  if numLines == 0:
    print("\nNo lines of symmetry.\n")

    return
  elif numLines == 1:
    lineStr = "line"

  print()
  print(numLines, lineStr, "of symmetry:")

  for l in lines:
    if l.isLineOfSymmetry:
      l.print()
    
  print()

def find_symmetry_lines(title, vertices):
  print("\n*** " + title + " ***\n\nVertices:")
  print(vertices)

  lines = []

  if len(vertices) < 2 or equal(vertices):
    printLinesFound(lines)
    plot(title, vertices, lines)

    return

  centroid = vertices.mean(axis=0)
  midpoints = find_midpoints(vertices)

  for p in np.concatenate((vertices, midpoints)):
    # get line that has not been tested yet
    line, hasBeenTested = get_line(p, centroid, lines)
    if hasBeenTested:
      continue

    line = test_line(title, line, vertices)
    lines.append(line)
  
  # for collinear vertices, test perpendicular line intersecting centroid
  line, areCollinear = collinear(vertices)
  if areCollinear: 
    pLine = line.compute_perpendicular(centroid)
    pLine = test_line(title, pLine, vertices)
    lines.append(pLine)
  
  printLinesFound(lines)

  # plot results
  plot(title, vertices, lines, centroid, midpoints)

  return lines

def randomIntVertices(lowerBound, upperBound, count):
  vertices = []

  for i in range(count):
    x = randint(lowerBound, upperBound)
    y = randint(lowerBound, upperBound)
    vertices.append([x, y])

  return np.array(vertices)
