# symmetry
### Description
This is a python project that computes lines of symmetry for groups of vertices. I used built-in math packages sparingly so I could be sure I knew what was happening at each step.

### Install
I believe the only package I had to install was `python-tk` using `brew install python-tk`.

### Run
* Run the python tests.py script to see the results of the test input vertices I included.
* A plot will appear showing vertices (blue dots) and lines of symmetry (red dotted lines).
* The console will print the name of the test along with vertex locations and the number of lines of symmetry as well as specifics about each line.
* A keyboard input is required to move between tests.
* There is a global var called `debug` that is set at the top of tests.py that can be set to `True` to see the inner-workings of the algorithm, including midpoint calculation and reflected vertex testing for line of symmetry candidates.  This will plot each step as well as print additional debug text and will take much longer to finish all tests.

### Testing
Feel free to add additional tests or remove existing ones.  I tested everything
I could think of, including sets of randomly generated (integer-valued) vertices.

### Submission Date
Written for Nautilus Biotechnology

Monday April 22, 2024

Greg Grant