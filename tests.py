import basic_tests
import edge_case_tests
import symmetry
import random_tests

# set global var
symmetry.debug = False

# run tests
basic_tests.run()
edge_case_tests.run()
random_tests.run()