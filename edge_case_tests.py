import numpy as np
import symmetry

def run():
  symmetry.find_symmetry_lines(
    "No Vertices",
    np.array([]),
  )

  symmetry.find_symmetry_lines(
    "One Vertex",
    np.array([[0, 0]]),
  )

  symmetry.find_symmetry_lines(
    "Two Vertices",
    np.array([[0, 0], [1, 1]]),
  )

  symmetry.find_symmetry_lines(
    "Three Collinear Vertices",
    np.array([[0, 0], [1, 1], [2, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Three Vertical Collinear Vertices",
    np.array([[0, 0], [0, 1], [0, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Three Horizontal Collinear Vertices",
    np.array([[0, 0], [1, 0], [2, 0]]),
  )

  symmetry.find_symmetry_lines(
    "Three Collinear Vertices with Two Equal",
    np.array([[0, 0], [1, 1], [0, 0]]),
  )

  symmetry.find_symmetry_lines(
    "Four Collinear Vertices with Two Equal Off-Center",
    np.array([[0, 0], [1, 1], [2, 2], [0, 0]]),
  )

  symmetry.find_symmetry_lines(
    "Three Equal Vertices",
    np.array([[0, 0], [0, 0], [0, 0]]),
  )

  symmetry.find_symmetry_lines(
    "Square with Duplicate Lower Right Vertex",
    np.array([[0, 0], [2, 0], [2, 2], [2, 0], [0, 2]]),
  )

  symmetry.find_symmetry_lines(
    "Square with Duplicate Right Side Vertices",
    np.array([[0, 0], [2, 0], [2, 2], [2, 0], [2, 2], [0, 2]]),
  )
